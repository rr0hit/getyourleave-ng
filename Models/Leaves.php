<?
require_once("Query.php");

class Leaves {

    static function reallocateManager($oldManID, $newManID) {
        $res = Query::update("UPDATE leaves
                                    SET manID = $newManID
                                    WHERE manID = $oldManID
                                    AND status = 'pending';");
        return $res;
    }

    static function updateStatus($leaveID, $status, $remarks) {
        $res = Query::update("UPDATE leaves
                                    SET status = '$status',
                                    remarks = '$remarks',
                                    isNotified = 0
                                    WHERE leaveID = $leaveID;");
        return $res;
    }

    static function getEmployeeID($leaveID) {
        $res = Query::select("SELECT empID 
                                    FROM leaves
                                    WHERE leaveID = $leaveID");
        $row = $res[0];
        return $row['empID'];
    }
    
    static function getManagerID($leaveID) {
        $res = Query::select("SELECT manID 
                                    FROM leaves
                                    WHERE leaveID = $leaveID");
        $row = $res[0];
        return $row['manID'];
    }

    static function newLeave($empID, $type, $reason, $fromDate, $toDate) {
        $res = Query::insert("INSERT INTO leaves 
                                    (empID, manID, fromDate, toDate, type, reason, appliedOn) 
                                    VALUES ($empID, (SELECT manID FROM employees WHERE empID = $empID), 
                                    '$fromDate', '$toDate', '$type', '$reason', now());");

        return $res;
    }

    static function getDetails($leaveID) {
        $res = Query::select("SELECT leaves.*, 
                                    emp.name as applicant, 
                                    man.name as manager
                                    FROM leaves
                                    INNER JOIN employees as emp
                                    ON emp.empID = leaves.empID
                                    INNER JOIN employees as man
                                    ON man.empID = leaves.manID
                                    WHERE leaves.leaveID = $leaveID;");
        $row = $res[0];
        return $row;
    }

    static function deleteLeave($leaveID) {
        $res = Query::update("UPDATE leaves SET isActive = 0 
                                    WHERE leaveID = $leaveID;");
        return $res;
    }

    static function getLeavesByEmployee($empID) {
        $res = Query::select("SELECT leaves.*, 
                                    man.name as manager, 
                                    emp.name as applicant 
                                    FROM leaves 
                                    INNER JOIN employees as man 
                                    ON man.empID = leaves.manID 
                                    INNER JOIN employees as emp 
                                    ON emp.empID = leaves.empID 
                                    WHERE leaves.empID = $empID
                                    AND leaves.isActive = 1
                                    ORDER BY fromDate DESC;");
        return $res;
    }

    static function getAcceptedLeavesByEmployee($empID) {
        $res = Query::select("SELECT leaves.*, 
                                    man.name as manager, 
                                    emp.name as applicant 
                                    FROM leaves 
                                    INNER JOIN employees as man 
                                    ON man.empID = leaves.manID 
                                    INNER JOIN employees as emp 
                                    ON emp.empID = leaves.empID 
                                    WHERE leaves.empID = $empID
                                    AND leaves.status = 'accepted' 
                                    AND leaves.isActive = 1
                                    AND leaves.type != 'work from home' 
                                    ORDER BY fromDate DESC;");
        return $res;
    }

    static function getApplicationsToManager($manID) {
        $res = Query::select("SELECT leaves.*, 
                                    man.name as manager, 
                                    emp.name as applicant 
                                    FROM leaves 
                                    INNER JOIN employees as man 
                                    ON man.empID = leaves.manID 
                                    INNER JOIN employees as emp 
                                    ON emp.empID = leaves.empID 
                                    WHERE leaves.manID = $manID
                                    AND leaves.isActive = 1
                                    ORDER BY fromDate DESC;");
        return $res;
    }

    static function getApplicationsToManagerByStatus($manID, $status) {
        $res = Query::select("SELECT leaves.*, 
                                    man.name as manager, 
                                    emp.name as applicant 
                                    FROM leaves 
                                    INNER JOIN employees as man 
                                    ON man.empID = leaves.manID 
                                    INNER JOIN employees as emp 
                                    ON emp.empID = leaves.empID 
                                    WHERE leaves.manID = $manID
                                    AND leaves.status = '$status'
                                    AND leaves.isActive = 1
                                    ORDER BY fromDate DESC;");
        return $res;
    }

}

?>
