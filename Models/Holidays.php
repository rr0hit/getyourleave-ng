<?
require_once("Query.php");

class Holidays {
    static function getHolidays() {
        $res = Query::select("SELECT * FROM holidays ORDER BY date DESC;");
        return $res;
    }

    static function addHoliday($date, $name) {
        $res = Query::insert("INSERT INTO holidays 
                                    (date, name)
                                    VALUES ('$date', '$name');");
    }

    static function deleteHoliday($id) {
        $res = Query::update("DELETE FROM holidays WHERE holidayID = $id");
    }

    static function addHolidayList($array) {
        $stmt = "INSERT INTO holidays (date, name) VALUES";
        foreach ($array as $k=>$v) {
            $stmt = $stmt . " ('" . $v[0] . "', '" . addslashes($v[1]) . "') ,";
        }
        $stmt = substr($stmt, 0, strlen($stmt) - 2) . ";";
        //echo $stmt;
        $res = Query::insert($stmt);
    }

    static function getHolidaysBetween($from, $to) {
        $res = Query::select("SELECT date FROM holidays WHERE date >= '$from' AND date <= '$to';");
        return $res;
    }
}

?>
