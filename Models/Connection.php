<?php

class Connection{
    private static $config = json_decode(file_get_contents("/home/capillary/connectToDatabase.json"));
    private static $conn = new mysqli($config->host,$config->username,$config->password,$config->database);

    public static getDatabaseConnection() {
        return $conn;
    }
}

?>
