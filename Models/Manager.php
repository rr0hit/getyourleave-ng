<?
require_once("Query.php");

class Manager {

    static function getApplications($manID) {
        $res = Query::select("SELECT leaves.*, 
                                    man.name as manager, 
                                    emp.name as applicant 
                                    FROM leaves 
                                    INNER JOIN employees as man 
                                    ON man.empID = leaves.manID 
                                    INNER JOIN employees as emp 
                                    ON emp.empID = leaves.empID 
                                    WHERE leaves.manID = $manID
                                    AND leaves.isActive = 1;");
        return $res;
    }

    static function getApplicationsByStatus($manID, $status) {
        $res = Query::select("SELECT leaves.*, 
                                    man.name as manager, 
                                    emp.name as applicant 
                                    FROM leaves 
                                    INNER JOIN employees as man 
                                    ON man.empID = leaves.manID 
                                    INNER JOIN employees as emp 
                                    ON emp.empID = leaves.empID 
                                    WHERE leaves.manID = $manID
                                    AND leaves.status = '$status'
                                    AND leaves.isActive = 1;");
        return $res;
    }

}
?>
