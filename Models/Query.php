<?php 
 
class Query{
    private static $config;
    private static $conn; 
    
    public static function init() {
        self::$config = json_decode(file_get_contents("/home/capillary/connectToDatabase.json"));
        self::$conn = new mysqli(self::$config->host,self::$config->username,self::$config->password,self::$config->database);       
    }
    public function select($statement) {
        $ret = Array();
        $res = self::$conn->query($statement);
        if (!$res)
            return false;
        else {
            while($row = $res->fetch_assoc()) {
                $ret[] = $row;
            }
            return $ret;
        }
    }

    public function insert($statement) {
        $res = self::$conn->query($statement);
        if($res)
            return $res->insert_id;
        return false;
    }

    public function update($statement) {
        $res = self::$conn->query($statement);
        return self::$conn->affected_rows;
    }
}

Query::init();

?>
