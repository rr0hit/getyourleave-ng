<?
require_once("Query.php");

class Configs {
    static function getLeavesPerMonth() {
        $res = Query::select("SELECT leavesPerMonth FROM config;");
        return $res[0];
    }

    static function setLeavesPerMonth($leavesPerMonth) {
        $res = Query::update("UPDATE config SET leavesPerMonth = $leavesPerMonth;");
        return $res;
    }

    static function getTemplates() {
        $res = Query::select("SELECT newLeaveTemplate, updateLeaveTemplate FROM config;");
        return $res[0];
    }

    static function setTemplates($new, $update) {
        $res = Query::update("UPDATE config SET newLeaveTemplate = '$new', updateLeaveTemplate = '$update';");
    }
}
