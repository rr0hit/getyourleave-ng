<?

require_once("Query.php");

class Employees {

    static function getSessionData($email) {
        $res = Query::select("SELECT password, empID, name, type FROM employees WHERE email = '$email';");
        return $res[0];
    }

    static function getEmployees() {
        $res = Query::select("SELECT emp.*,
                                    man.name as manager
                                    FROM employees as emp
                                    INNER JOIN employees as man
                                    ON man.empID = emp.manID
                                    WHERE emp.isActive = 1;");
        return $res;
    }

    static function getEmployeesByName($search) {
        $res = Query::select("SELECT emp.*,
                                    man.name as manager
                                    FROM employees as emp
                                    INNER JOIN employees as man
                                    ON man.empID = emp.manID
                                    WHERE emp.isActive = 1
                                    AND emp.name LIKE '$search';");
        return $res;
    }

    static function addEmployee($name, $email, $password, $type, $manID) {
        $res = Query::insert("INSERT INTO employees 
                                    (name, email, password, type, manID, joiningDate) 
                                    VALUES ('$name', '$email', '$password', '$type', $manID, now());");
    }

    static function reallocateManager($oldManID, $newManID) {
        $res = Query::update("UPDATE employees
                                    SET manID = $newManID
                                    WHERE manID = $oldManID;");
        return $res;
    }

    static function deleteEmployee($empID) {
        $res = Query::update("UPDATE employees
                                    SET isActive = 0
                                    WHERE empID = $empID;");
    }

    static function getManager($empID) {
        $res = Query::select("SELECT manID FROM employees
                                    WHERE empID = $empID;");
        $row = $res[0];
        return $row['manID'];
    }

    static function setManager($empID, $manID) {
        $res = Query::update("UPDATE employees 
                                    SET manID = $manID
                                    WHERE empID = $empID;");
        $row = $res[0];
        return $row['manID'];
    }

    static function makeManager($empID) {
        $res = Query::update("UPDATE employees 
                                    SET type = 'manager'
                                    WHERE empID = $empID;");
    }

    static function demoteManager($empID) {
        $res = Query::update("UPDATE employees 
                                    SET type = 'employee'
                                    WHERE empID = $empID;");
    }

    static function deleteLeaves($empID) {
        $res = Query::update("UPDATE leaves
                                    SET isActive = 0
                                    WHERE empID = $empID;");
    }

    static function getTeam($manID) {
        $res = Query::select("SELECT employees.*, 
                                    man.name as manager
                                    FROM employees
                                    INNER JOIN employees as man 
                                    ON man.empID = employees.manID
                                    WHERE employees.manID = $manID
                                    AND employees.isActive = 1;");
        return $res;
    }

    static function getTeamFilterByName($manID, $search) {
        $res = Query::select("SELECT employees.*, 
                                    man.name as manager
                                    FROM employees
                                    INNER JOIN employees as man 
                                    ON man.empID = employees.manID
                                    WHERE employees.manID = $manID
                                    AND employees.name LIKE '$search'
                                    AND employees.isActive = 1;");
        return $res;
    }
}

?>
