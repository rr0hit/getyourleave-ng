<?
echo "
    <div class='span4 offset1 well'>
        <table>";
if ($isManager)
echo       "<tr>
                <td class='span2'>
                    <strong>Employee</strong>
                </td>
                <td class='span2'>".$row['applicant']."</td>
            </tr>";
echo       "<tr>
                <td class='span2'>
                    <strong>From</strong>
                </td>
                <td class='span2'>
                    ".$row['fromDate']."
                </td>
            </tr>
    		<tr>
                <td>
                    <strong>To</strong>
                </td>
                <td>".$row['toDate']."</td>
            </tr>
    		<tr>
                <td>
                    <strong>Type</strong>
                </td>
                <td>".$row['type']."</td>
            </tr>
    		<tr>
                <td>
                    <strong>Reason</strong>
                </td>
                <td>".$row['reason']."</td>
            </tr>";
if(! $isManager)
echo       "<tr>
                <td>
                    <strong>Manager</strong>
                </td>
                <td>".$row['manager']."</td>
            </tr>";
echo       "<tr>
                <td>
                    <strong>Status</strong>
                </td>
                <td>".$row['status']."</td>
            </tr>
		    <tr>
                <td>
                    <strong>Remarks</strong>
                </td>
                <td>".$row['remarks']."</td>
            </tr>
    		<tr>
                <td>
                    <strong>Applied On</strong>
                </td>
                <td>".$row['appliedOn']."</td>
            </tr>
		    <tr>
                <td>
                    <strong>Leaves taken</strong>
                </td>
                <td>0</td>
            </tr>
    </table>";
?>
