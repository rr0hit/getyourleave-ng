<?
    $type = $_SESSION["type"];

	echo  "
		<div class='span3 well' style='max-width: 200px; padding: 8px 0;'>
		<ul id='employeeMenu' class='nav nav-list'>";

	if($type != "administrator") {
		echo "
	    <li><a href='#' id='viewAppliedLeaves'>View applied leaves</a></li>
			<li><a href='#' id='applyForLeave'>Apply for leave</a></li>";
	}

	if($type == "manager" || $type == "administrator"){
		echo "
         <li class='divider'></li>
		<li><a href='#' id='viewTeam'>View Team</a></li>
		<li><a href='#' id='viewPendingRequests'>View pending requests</a></li>
		<li><a href='#' id='viewAcceptedRequests'>View accepted requests</a></li>
		<li><a href='#' id='viewRejectedRequests'>View rejected requests</a></li>";
	}

	if($type == "administrator"){
		echo "
         <li class='divider'></li>
        <li><a href='#' id='viewEmployees'>View Employee list</a></li>
		<li><a href='#' id='addEmployee'>Add a new employee</a></li>
        <li><a href='#' id='viewHolidays'>View holiday list</a></li>
		<li><a href='#' id='addHolidayList'>Upload holiday list</a></li>
		<li><a href='#' id='addAHoliday'>Add a Holiday</a></li>
        <li><a href='#' id='viewReminderPolicy'>View remainder policies</a></li>
		<li><a href='#' id='addReminderPolicy'>Add Remainder Policy</a></li>
		<li><a href='#' id='noOfLeavesPerMonth'>Set leaves per month</a></li>
		<li><a href='#' id='setLeaveUpdateTemplate'>Set update template</a></li>
		";
	}

	echo "<li class='divider'></li><li><a href='Controllers/logout.php' id='logout'>logout</a></li></ul></div>";

?>

