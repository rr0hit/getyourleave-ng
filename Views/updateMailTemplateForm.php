<?

$newLeaveTemplate = $row['newLeaveTemplate'];
$updateLeaveTemplate = $row['updateLeaveTemplate'];

echo "
<form method='post' action='Controllers/updateMailTemplates.php' class='form-horizontal'>
    <div class='control-group'>
        <label class='control-label'>New Leave Template</label>
        <div class='controls'> 
            <textarea type='text' rows='7' name='newLeaveTemplate'>$newLeaveTemplate</textarea>
        </div>
    </div>
    <div class='control-group'>
        <label class='control-label'>Leave Update Mail Template</label>
        <div class='controls'> 
            <textarea type='text' rows='7' name='updateMailTemplate'>$updateLeaveTemplate</textarea>
        </div>
    </div>
</form>
";

?>
