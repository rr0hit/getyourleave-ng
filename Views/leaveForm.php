<script src='js/datePicker.js'></script>
<form method='post' action='Controllers/applyForLeave.php' after-add='viewAppliedLeaves' class='form-horizontal'>
    <div class='control-group'>
		<label class='control-label'>From date</label>
            <div class='controls'>                    
                <input type='text' name='fromDate' class='span4 datepicker' id='fromDate' />
            </div>
    </div>
    <div class='control-group'>
		<label class='control-label'>To date</label>
            <div class='controls'>                    
                <input type='text' name='toDate' class='span4 datepicker' id='toDate' />
            </div>
    </div>
    <div class='control-group'>
		<label class='control-label'>Type</label> 
            <div class='controls'>                    
                <select type='text' name = 'type' class='span4' id='type'>
	    			<option>sick</option>
	    			<option>planned</option>
	    			<option>work from home</option>
				</select>
            </div>
    </div>
    <div class='control-group'>
		<label class='control-label'>Reason</label> 
            <div class='controls'>                    
                <textarea type='text' name='reason' class='span4' id='reason' />
            </div>
    </div>
</form>
