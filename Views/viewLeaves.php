<?
echo "<table class='table table-striped table-bordered'><tr>";
	if($isManager) echo "<th class='span2'>Employee</th>";
	else echo	"<th class='span4'>Manager</th>";
	echo "<th>From Date</th><th>To Date</th><th class='span1'>Working days</th><th>Status</th>";
	if($isManager) echo "<th>Take Action</th>";
    if(! $isManager) echo "<th></th>";
	echo	"<th></th></tr>";

	foreach($leaveArray as $k=>$v){
        $today = new DateTime();
        $afterToday = true;
        if (date_create($v['fromDate']) < $today) {
            $afterToday = false;
        }
	    echo "<tr>";
	    if($isManager){
		    $cellValue = $v['applicant'];
	    }
	    else {
		    $cellValue = $v['manager'];
	    }
	    echo "<td>$cellValue</td>";
	    $from = $v['fromDate'];
    	echo "<td>$from</td>";
	    $to = $v['toDate'];
	    echo "<td>$to</td>";
        require_once("../Controllers/leavesCalculator.php");
        $wDays = getWorkingDays($from, $to);
	    $numberOfDays = sizeof($wDays);
	    echo "<td>$numberOfDays</td>";
	    $status = $v['status'];
	    echo "<td>$status</td>";
	    $leaveID = $v['leaveID'];
	    if($isManager) {
		    echo "<td>";
		    if($status!="accepted") 
                if ($afterToday) echo "<button action='Accept' class='statusbtn btn btn-success' style='margin: 0px 10px' href='Controllers/updateStatus.php?lid=$leaveID&status=accept'>accept</button>";
                else echo "<button class='btn btn-success disabled' style='margin: 0px 10px' >accept</button>";
		    if($status!="rejected") 
                if ($afterToday) echo "<button action='Reject' class='statusbtn btn btn-danger' style='margin: 0px 10px' href='Controllers/updateStatus.php?lid=$leaveID&status=reject'>reject</button>";
                else echo "<button class='btn btn-danger disabled' style='margin: 0px 10px' >reject</button>";
		    echo "</td>";
	    }
        echo "<td><button class='details btn btn-info' style = 'margin: 0px 10px' href='Controllers/leaveDetails.php?lid=$leaveID'>Details</button></td>";
        if (! $isManager) {if($afterToday)    echo "<td><button class='delbtn btn btn-danger' after-del='viewAppliedLeaves' style = 'margin: 0px 10px' href='Controllers/cancelApplication.php?lid=$leaveID'>Cancel</button></td>";
        else echo "<td><button class='btn btn-danger disabled' style = 'margin: 0px 10px'>Cancel</button></td>";}
        
	
        echo "</tr>";
	}

	echo "</table>";
?>
