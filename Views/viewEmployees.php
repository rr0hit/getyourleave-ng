<?
if($isAdmin) {
    $target = 'Controllers/employees.php';
    $afterSearch = '#viewEmployee';
}
else {
    $target = 'Controllers/team.php';
    $afterSearch = '#viewTeam';
}

include("search.php");

echo "<table class='table table-striped table-bordered'>\n" . 
      "\t<tr>\n" .
      "\t\t<th> Name </th>\n" .
      "\t\t<th> Email </th>\n" .
      "\t\t<th> Type </th>\n".
      "\t\t<th> Joining Date </th>\n" .
      "\t\t<th> Leaves Approved </th>\n" ;
  if($isAdmin) {
  	echo "\t\t<th>Manager</th>\n";
    echo "\t<th class='span3'></th>";
    echo "\t<th></th>";
  }
  
  echo "\t</tr>\n";
  require_once("../Controllers/leavesCalculator.php");
  foreach($empArray as $k => $v) {
    $lTaken = getLeavesTaken($v['empID']);
    echo "\t<tr>\n".
      "\t\t<td class='span2'> ". $v['name']. " </td>\n".
      "\t\t<td> ". $v['email']. " </td>\n".
      "\t\t<td>". $v['type']." </td>\n".
      "\t\t<td> " . $v['joiningDate']. " </td>\n".
      "\t\t<td> ". $lTaken . " </td>\n";

    if($isAdmin) {
    	$employeeID = $v['empID'];
    	$manName = $v['manager'];
    	echo "\t\t<td>".$manName."</td><td><button class='manbtn btn btn-primary' default='Change Manager' settext='Set as Manager' empID='$employeeID'></button>"."</td>\n";
    	echo "\t\t<td><button class='delbtn btn btn-danger' after-del='viewEmployees' href='Controllers/deleteEmployee.php?empID=$employeeID'>Delete</button></td>\n";
    }
    echo "\t</tr>\n";
  }
  echo "</table>";
  echo "<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>";

?>
