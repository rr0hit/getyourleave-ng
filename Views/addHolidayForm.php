<script src='js/datePicker.js'></script>

<form method='post' action='Controllers/addHoliday.php' after-add='viewHolidays' class='form-horizontal'>
    <div class='control-group'>
        <label class='control-label'>Holiday Date</label>
            <div class='controls'>
                <input type='text' name='holidayDate' class='datepicker'/>
            </div>
    </div>
    <div class='control-group'>
	    <label class='control-label'>Name of Holiday</label>
            <div class='controls'>
                <input type='text' name='nameOfHoliday'/>
            </div>
    </div>
</form>
