<?
    session_start();
    if($_SESSION['type'] == 'administrator') {
        require_once("../Models/Configs.php");
        if(! isset($_POST['newLeaveTemplate'])) {        
            $row = Configs::getTemplates();
            include("../Views/updateMailTemplateForm.php");
        }
        else {
            $newTemplate = addslashes($_POST['newLeaveTemplate']);
            $updateTemplate = addslashes($_POST['updateMailTemplate']);
            Configs::setTemplates($newTemplate, $updateTemplate);
            echo "ok"; 
        }
    }

    else {
        echo "Error: Not authorized!!";
    }
?>
