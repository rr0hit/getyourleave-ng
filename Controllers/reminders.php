<?
    session_start();
    if($_SESSION['type'] == 'administrator') {
        require_once("../Models/Reminders.php");        
        $reminderArray = Reminders::getReminders();
        include("../Views/viewReminders.php");
    }

    else {
        echo "Error: Not authorized!!";
    }
?>
