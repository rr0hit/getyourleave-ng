<?
    if(isset($_GET['status']) && isset($_GET['lid'])) {
        session_start();
        $today = new DateTime();
        require_once("../Models/Leaves.php");
        $leave = Leaves::getDetails($_GET['lid']);
        if ($_SESSION['empID'] != $leave['manID']) {
            echo "Error: Not authorized.";
            exit;
        }
        else if (date_create($leave['fromDate']) < $today) {
            echo "Error: You can no longer accept/reject this application!!";
            exit;
        }
        $afterAdd = "viewRejectedRequests";
        $isAccepted = false;    
        if($_GET['status'] == 'accept') {
            $afterAdd = "viewAcceptedRequests";
            $isAccepted = true;
        }
        $leaveID = $_GET['lid'];
        include("../Views/addRemarksForm.php");
    }
	
?>
