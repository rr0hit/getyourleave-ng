<?

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }

session_start();
if($_SESSION['type'] == 'administrator') {
    if (isset($_POST['name']) && isset($_POST['email'])) {
        if(empty($_POST['name']))
        {
          echo "Error: Please provide a valid name";
          die();
        }
        if(empty($_POST['email']))
        {
          echo "Error: Please provide a valid email";
          die();
        }
        require_once("../Models/Employees.php");
        Employees::addEmployee($_POST['name'], $_POST['email'], generateRandomString(), 'employee', $_SESSION['empID']);
        echo "ok";
    }
    else {
        echo "Error: Malformed request.";
    }
}

else {
    echo "Error: Not authorized.";
}

?>
