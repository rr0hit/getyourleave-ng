<?
    require_once("../Models/Leaves.php");
    session_start();
    $isManager = true;
    if(isset($_GET['status'])) {
        $leaveArray = Leaves::getApplicationsToManagerByStatus($_SESSION['empID'], $_GET['status']);
    }
    else {
        $leaveArray = Leaves::getApplicationsToManager($_SESSION['empID']);
    }
    if($leaveArray)
        include('../Views/viewLeaves.php');
?>
