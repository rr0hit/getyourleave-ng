<?
    session_start();
    if($_SESSION['type'] == 'administrator') {
        require_once("../Models/Holidays.php");        
        $rows = Holidays::getHolidays();
        include("../Views/viewHolidays.php");
    }

    else {
        echo "Error: Not authorized!!";
    }
?>
