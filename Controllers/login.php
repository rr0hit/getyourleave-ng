<?php
require_once("../Models/Employees.php");
if($_POST["email"] == "" or $_POST["password"] == "") {
    header("location: ../index.php#error");
    exit;
}

$res = Employees::getSessionData($_POST["email"]);

if(!$res){
  header("location: ../index.php#error");
  exit;
}

else {
  if(!isset($_POST['password'])) {
    header("location: ../index.php#error");
    exit;
  }
  else {
    $empID = $res['empID'];
    $password = $res["password"];
    if($_POST["password"] == $password){
        session_start();
        $_SESSION["empID"] = $empID;
        $_SESSION["type"] = $res["type"];
        $_SESSION["name"] = $res["name"];
        header("location: ../dashboard.php");
        exit;
      }
      else{
        header("location: ../index.php#error");
        exit;
      }
  }
}
?>
