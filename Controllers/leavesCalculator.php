<?

function isWeekend($date) {
    return (date('N', $date) >= 6);
}

function getWorkingDays($from, $to) {
    $d = strtotime($from);
    $workingDays = Array();    
    while($d <= strtotime($to)) {
        if (!isWeekend($d))
            $workingDays[] = $d;
        $d = $d + 86400;    
    }
    require_once("../Models/Holidays.php");
    $hols = Holidays::getHolidaysBetween($from, $to);
    foreach($hols as $k => $h) {
        foreach($workingDays as $i=>$w)
            if($w == strtotime($h['date']))
                unset($workingDays[$i]);
    }
    return $workingDays;
}

function getLeavesTaken($empID) {
    require_once("../Models/Leaves.php");
    $lTaken = Leaves::getAcceptedLeavesByEmployee($empID);
    $total = 0;    
    foreach($lTaken as $i => $l) {
        $w = getWorkingDays($l['fromDate'], $l['toDate']);
        $total = $total + sizeof($w);
    }
    return $total;
}

function checkLeaveAvailability($empID, $from, $to) {

    require_once("../Models/Configs.php");
    $leavesPerMonth = Configs::getLeavesPerMonth();

    $leavesPerMonth = $leavesPerMonth["leavesPerMonth"];
    $wdays = getWorkingDays($from, $to);

    $leavesTaken = getLeavesTaken($empID);
    foreach ($wdays as $k => $day) {

        $leavesAvailable = date("m", $day) * $leavesPerMonth - $leavesTaken;

        if ($leavesAvailable > 0) {

            $leavesTaken = $leavesTaken + 1;

        }
        else {
            echo "Error: Not enough leaves.";
            return false;
        }

    }
    return true;     

}


?>
