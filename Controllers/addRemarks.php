<?
	require_once "../Models/Leaves.php";
	session_start();
    if (isset($_POST['lid']) && isset($_POST['status']) && isset($_POST['remarks'])) {
	    $manID = Leaves::getManagerID($_POST['lid']);
	    $remarks = $_POST['remarks'];
	    $leaveID = $_POST['lid'];
        if ($manID == $_SESSION['empID']) {
            Leaves::updateStatus($leaveID, $_POST['status'], $remarks);
            echo "ok";
        }
        else {
            echo "Error: Not authorized!!";
        }
    }
    else {
        echo "Error: Malformed request.";
    }
?>
