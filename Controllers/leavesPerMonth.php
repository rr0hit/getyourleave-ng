<?
    session_start();
    if($_SESSION['type'] == 'administrator') {
        require_once("../Models/Configs.php");
        if(! isset($_POST['leavesPerMonth'])) {        
            $row = Configs::getLeavesPerMonth();
            $currentLeavesPerMonth = $row['leavesPerMonth'];
            include("../Views/leavePerMonthForm.php");
        }
        else {
            if(is_numeric($_POST['leavesPerMonth'])) {
                Configs::setLeavesPerMonth($_POST['leavesPerMonth']);
                echo "ok";
            }
            else {
                echo "Error: Input must be a number.";
            }
        }
    }

    else {
        echo "Error: Not authorized!!";
    }
?>
