<?
require_once("../Models/Leaves.php");
session_start();
if($_SESSION['empID'] == Leaves::getEmployeeID($_GET['lid'])) {
    $today = new DateTime();
    $leave = Leaves::getDetails($_GET['lid']);
    if (date_create($leave['fromDate']) < $today) {
        echo "Error: You can no longer cancel this application!!";
    }
    else {
        Leaves::deleteLeave($_GET['lid']);
        echo "ok";
    }
}
else {
    echo "You are not authorized to perform this action.";
}
?>
