<?
session_start();
if($_SESSION['type'] == 'administrator') {
    if (isset($_POST['holidayDate']) && isset($_POST['nameOfHoliday'])) {
        if(!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $_POST['holidayDate'])) {
            echo "Error: Wrong date format.";
            exit;
        }
        require_once("../Models/Holidays.php");
        $hname = addslashes($_POST['nameOfHoliday']);
        Holidays::addHoliday($_POST['holidayDate'], $hname);
        echo "ok";
    }
    else {
        echo "Error: Malformed request.";
    }
}

else {
    echo "Error: Not authorized.";
}

?>
