<?
session_start();
if($_SESSION['type'] == 'administrator') {
    if (isset($_GET['rid'])) {
        require_once("../Models/Reminders.php");
        Reminders::deleteReminder($_GET['rid']);
        echo "ok";
    }
    else {
        echo "Error: Malformed request.";
    }
}

else {
    echo "Error: Not authorized.";
}

?>
