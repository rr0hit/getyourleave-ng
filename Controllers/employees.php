<?

session_start();

if($_SESSION['type'] == 'administrator') {
    $isAdmin = true;
    require_once "../Models/Employees.php";
    if(isset($_GET['search']))
        $empArray = Employees::getEmployeesByName($_GET['search'] . '%');
    else
        $empArray = Employees::getEmployees();
    if($empArray)
        include('../Views/viewEmployees.php');
}

?>
