<?
session_start();
if ($_SESSION["type"] == 'administrator') {
    if(isset($_FILES["csv_file"])) {
        if($_FILES["csv_file"]["type"] == "text/csv") {
            $csv_file = fopen($_FILES["csv_file"]["tmp_name"], "r");
            $hols_array = Array();
            $acceptable = true;
            while($csv_array = fgetcsv($csv_file)) {
                if(!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $csv_array[0])) {
                    $acceptable = false;
                    break;
                }
                else {
                    $hols_array[] = $csv_array;
                }
            }
            if ($acceptable) {
                require_once("../Models/Holidays.php");
                Holidays::addHolidayList($hols_array);
                echo "ok";
            }
            else {
                echo "Error: One or more bad lines in file.";
            }
        }
        else {
            echo "Error: Incorrect file type.";
        }
    }

    else {
        echo "Error: No file uploaded.";
    }
}
?>
