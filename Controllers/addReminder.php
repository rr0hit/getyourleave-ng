<?

session_start();
if($_SESSION['type'] == 'administrator') {
    if (isset($_POST['daysBefore']) && isset($_POST['template']) && isset($_POST['ccList'])) {
        require_once("../Models/Reminders.php");
        if(is_numeric($_POST['daysBefore'])) {
            $template = addslashes($_POST['template']);
            Reminders::addReminder($_POST['daysBefore'], $template, $_POST['ccList']);
            echo "ok";
        }
        else {
            echo "'Days Before' must be a number";
        }
    }
    else {
        echo "Error: All fields are required.";
    }
}

else {
    echo "Error: Not authorized.";
}

?>
