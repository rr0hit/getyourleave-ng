<?

if (isset($_GET['lid'])) {
    require_once("../Models/Leaves.php");
    session_start();
    $row = Leaves::getDetails($_GET['lid']);
    if($_SESSION['empID'] == $row['manID'])
        $isManager = true;
    else if($_SESSION['empID'] == $row['empID'])
        $isManager = false;
    else {
        echo "Error: Not authorized.";
        exit;
    }
    include("../Views/viewDetails.php");
}
else {
    echo "Error: Malformed request.";
}
?>
