<?
session_start();

if($_SESSION['type'] == 'administrator') {
    require_once("../Models/Employees.php");
    function detectCycle($empID, $manID) {
        $isCycle = false;
        while($manID != $_SESSION['empID']) {
            if ($manID == $empID) {
                $isCycle = true;
                break;
            }
            $manID = Employees::getManager($manID);
        }

        return $isCycle;
    }

    if (isset($_POST['manID'])) {
	    if (isset($_POST["empID"])) {
	        $manID = $_POST['manID'];
	        
            if ($_POST["empID"] == $_SESSION["empID"]) {
                echo "Admin's manager is always admin. Sorry!!";
            }
            else if(detectCycle($_POST['empID'], $manID)) {
                echo "Cycle detected!!";
            }
            else {
                $oldManager = Employees::getManager($_POST['empID']);
                $oldManTeam = Employees::getTeam($oldManager);
                if(sizeof($oldManTeam) == 1) {
                    if($oldManager != $_SESSION['empID'])
                        Employees::demoteManager($oldManager);
                }
	        	Employees::setManager($_POST['empID'], $manID);
                if($manID != $_SESSION['empID'])
                    Employees::makeManager($manID);
                echo "ok";
            }
	    }
    }

    else {
	    echo "Error: Malformed request.";
    }
}

else
    echo "You are not authorized to view this page!!";

?>
