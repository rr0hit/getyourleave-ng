<?
session_start();
if($_SESSION['type'] == 'administrator') {
    if (isset($_GET['hid'])) {
        require_once("../Models/Holidays.php");
        Holidays::deleteHoliday($_GET['hid']);
        echo "ok";
    }
    else {
        echo "Error: Malformed request.";
    }
}

else {
    echo "Error: Not authorized.";
}

?>
