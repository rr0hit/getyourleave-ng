<?
session_start();

if($_SESSION['type'] == 'administrator') {

    function delete($id) {
        require_once("../Models/Employees.php");
        require_once("../Models/Leaves.php");
        Employees::deleteEmployee($id);
        $newMan = Employees::getManager($id);
        Employees::reallocateManager($id, $newMan);
        Leaves::reallocateManager($id, $newMan);
        Employees::deleteLeaves($id);
        echo "ok";
    }

    if (isset($_GET["empID"])) 
        delete($_GET["empID"]);

    else if (isset($_POST["empID"]))
        delete($_POST["empID"]);

    else {
        echo "Error: Malformed request";
    }
}

else
    echo "You are not authorized to view this page!!";

?>
