<?php
    session_start();
    if(! isset($_SESSION['empID'])) {
        header('location: ../index.php');
    }
	if($_POST["fromDate"] != "" && $_POST["toDate"] != "" && $_POST["type"] != ""){
		include_once "../Models/Leaves.php";
		$today = new DateTime();
        if(!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $_POST['fromDate'])) {
            echo "Error: Wrong date format.";
        }
        else if(!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $_POST['toDate'])) {
            echo "Error: Wrong date format.";
        }
        else if(date_create($_POST["toDate"]) < date_create($_POST["fromDate"])) {
            echo "Error: \"From date\" must be before \"To date\".";
            exit;
        }
        else if(date_create($_POST['fromDate']) < $today || date_create($_POST['toDate'] < $today)) {
        	echo "Error: \"From date\" cannot be less that today";
        }
        else {
        
        require_once("../Controllers/leavesCalculator.php");

        if(checkLeaveAvailability($_SESSION['empID'], $_POST["fromDate"], $_POST["toDate"])) {

            Leaves::newLeave($_SESSION['empID'], $_POST['type'], $_POST['reason'], $_POST["fromDate"], $_POST["toDate"]);
            echo "ok";
        }

        }
	}
	else {
		echo "Error: Bad request.";
	}
	
?>
