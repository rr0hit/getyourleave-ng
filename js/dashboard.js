var setmode = false;
var selectedEmp = 0;
function checkerror(data) {
    if (data != "ok") {
        $('.modal').modal('show');
        $('#popup').html('');
        $('.alert').html(data);
        $('.alert').show();
        $('#savebtn').hide();
    }
}

function loadToMainContent(page, choice, data) {
   $('.active').removeClass('active');
   $("#mainContent").load(page, data, function (){
        $('.details').click(function (){
            $('#popuptitle').html("Leave Details");
            loadToPopUp($(this).attr('href'), $(this).parent());
            $('#savebtn').hide();
        });

        $('.statusbtn').click(function () {
            $('#popuptitle').html( $(this).attr('action') +" Leave Application");
            loadToPopUp($(this).attr('href'), $(this).parent());
        });

        $('.delbtn').click(function () {
            var thisbtn = $(this);
            $.post($(this).attr('href'), {}, function (data) {
                checkerror(data);
                $('#'+thisbtn.attr('after-del')).trigger('click');            
            });
        });
        
        $('#namesearchform').submit(function (e) {
            e.preventDefault();
        });

        $('#namesearchbtn').click(function () {
            if($('.manbtn').html() != $('.manbtn').attr('default'))
                setmode = true;
            loadToMainContent($('#namesearchform').attr('target'), $($(this).attr('after-search')).parent(), $("#namesearchform").serialize());       
        });

        if(setmode) {
            $('.manbtn').html($('.manbtn').attr('settext'));
            $('.manbtn').removeClass('btn-standard');
            $('.manbtn').addClass('btn-success');
            setmode = false;
        }
        else {
            $('.manbtn').html($('.manbtn').attr('default'));
            $('.manbtn').removeClass('btn-success');
            $('.manbtn').addClass('btn-standard');
        }

        $('.manbtn').click(function () {
            if($('.manbtn').html() == $('.manbtn').attr('default')) {
                selectedEmp = $(this).attr('empID');
                $('.manbtn').html($('.manbtn').attr('settext'));
                $('.manbtn').removeClass('btn-standard');
                $('.manbtn').addClass('btn-success');
            }

            else {
                $.post("Controllers/changeManager.php", {'empID': selectedEmp, 'manID': $(this).attr('empID')}, function(data) {
                    setmode = false;                
                    $('#viewEmployees').trigger('click');
                    checkerror(data); 
                });
            }
        });
    });
   choice.addClass('active');
}

var isFileupload = false;

function loadToPopUp(page, choice, isFile) {
    isFileupload = typeof isFile !== 'undefined' ? isFile : false;
    
    $('.alert').hide();
    $('.active').removeClass('active');
    $('.modal').modal('show');
    $('#popup').load(page, function() {
        if(isFileupload) {
            $('#popup').find('form').ajaxForm(function (data) {
                if(data == "ok") {
                    $('.modal').modal('hide');
                    $('#' + $('#popup').find('form').attr('after-add')).trigger('click');
                }
    
                else {
                    $('.alert').show();
                    $('.alert').html(data + "<a class='closealert close' href='#'>&times;</a>");
                    $('.closealert').click( function () {
                        $('.alert').hide();        
                    });
                }
            });
        }    
        choice.addClass('active');
    });    
}

$('document').ready(function() { 
    $('.popover').hide();
    $('.alert').hide();

    $('.modal').on('hidden', function () {
        $('#savebtn').show();
    })

    $('#viewTeam').click(function(){
        loadToMainContent("Controllers/team.php", $(this).parent());
        
    });

    $('#viewPendingRequests').click(function(){
        loadToMainContent("Controllers/LeaveApplications.php?status=pending", $(this).parent());
    });

    $('#viewAcceptedRequests').click(function(){
        loadToMainContent("Controllers/LeaveApplications.php?status=accepted", $(this).parent());
    });

    $('#viewRejectedRequests').click(function(){
        loadToMainContent("Controllers/LeaveApplications.php?status=rejected", $(this).parent());
    });

    $('#viewAppliedLeaves').click(function(){
        loadToMainContent("Controllers/AppliedLeaves.php", $(this).parent());
    });

    $('#viewAppliedLeaves').trigger("click");

    $('#viewEmployees').click(function(){
        loadToMainContent("Controllers/employees.php", $(this).parent());
    });

    $('#viewHolidays').click(function(){
        loadToMainContent("Controllers/holidays.php", $(this).parent());
    });

    $('#viewReminderPolicy').click(function(){
        loadToMainContent("Controllers/reminders.php", $(this).parent());
    });

    $('#applyForLeave').click(function(){
        $('#popuptitle').html("Apply For Leave");
        loadToPopUp("Views/leaveForm.php", $(this).parent());
    });

    $('#addEmployee').click(function(){
        $('#popuptitle').html("Add Employee");
        loadToPopUp("Views/addEmployeeForm.php", $(this).parent());
    });

    $('#addHolidayList').click(function(){
        $('#popuptitle').html("Add Holidays");
        loadToPopUp("Views/uploadcsvForm.php", $(this).parent(), true);
    });

    $('#addAHoliday').click(function(){
        $('#popuptitle').html("Add Holidays");
        loadToPopUp("Views/addHolidayForm.php", $(this).parent());
    });

    $('#addReminderPolicy').click(function(){
        $('#popuptitle').html("Add Reminder Policy");
        loadToPopUp("Views/addReminderForm.php", $(this).parent());
    });

    $('#noOfLeavesPerMonth').click(function(){
        $('#popuptitle').html("Set leaves per month");
        loadToPopUp("Controllers/leavesPerMonth.php", $(this).parent());
    });

    $('#setLeaveUpdateTemplate').click(function(){
        $('#popuptitle').html("Set leave update templates");
        loadToPopUp("Controllers/updateMailTemplates.php", $(this).parent());
    });

    $('#savebtn').click(function () {
        if (isFileupload) {
            $('#popup').find('form').submit();
            return;
        }
        $.post($('#popup').find('form').attr('action'), $("#popup").children().serialize(), function (data) {            
            if(data == "ok") {
                $('.modal').modal('hide');
                $('#' + $('#popup').find('form').attr('after-add')).trigger('click');
            }

            else {
                $('.alert').show();
                $('.alert').html(data + "<a class='closealert close' href='#'>&times;</a>");
                $('.closealert').click( function () {
                    $('.alert').hide();        
                });
            }
        });
    });

});
